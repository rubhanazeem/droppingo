Rails.application.routes.draw do

  get 'blogs/show'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, :controllers => { registrations: 'registrations', sessions: 'sessions' }
  root 'home#index'

  get 'users/edit_profile', to: 'users#edit_profile', as: :user_edit_profile
  get 'users/show/profile', to: 'users#show_profile', as: :user_profile
  put 'add_to_wish_list', to: 'users#add_to_wish_list', as: :add_to_wish_list
  get 'user/dashboard', to: 'users#user_dashboard', as: :user_dashboard
  get 'user/new_address', to: 'users#new_address', as: :new_address
  get 'user/address_book', to: 'users#address_book', as: :address_book
  get 'student_club', to: 'users#student_club', as: :student_club
  get 'user_lists', to: 'users#user_lists', as: :user_lists
  post 'user/lists', to: 'users#create_list', as: :create_list
  get 'user/:id/lists', to: 'users#edit_list', as: :edit_list
  delete 'remove/list/product/:id', to: 'users#remove_from_list', as: :remove_from_list
  get 'my/orders', to: 'users#show_orders', as: :show_orders
  get 'order/:id', to: 'users#order_details', as: :order_details
  post 'user/new/address', to: 'users#add_new_address', as: :add_new_address
  get 'edit/:id/address', to: 'users#edit_address', as: :edit_address
  patch 'edit/:id/address', to: 'users#update_address', as: :update_address
  delete 'remove/address/:id', to: 'users#destroy_address', as: :destroy_address
  put '/set/default_address', to: 'users#set_default_address'
  get 'edit/:id/student_profile', to: 'users#edit_student_club', as: :edit_student_club
  put 'edit/:id/student_profile', to: 'users#update_student_club', as: :update_student_club
  get 'reviews', to: 'users#show_reviews', as: :reviews
  
  resources :products , only:[:index, :show]
  resources :blogs, only:[:index, :show]

  get 'terms_and_conditions', to: "requests#terms_and_conditions"
  get 'privacy_policy', to: "requests#privacy_policy"
  get "new_book_request", to: "requests#book_request_new"
  post 'book_requests', to: "requests#book_request_create"
  get "study_abroad", to: "requests#study_abroad_new"
  post 'study_abroads', to: "requests#study_abroad_create"
  get 'publish_request', to: 'requests#publish_requests_new'
  post 'publish_requests', to: 'requests#publish_requests_create'
  get 'bulk_order_info', to: 'requests#bulk_order_info'
  get 'new/student/membership', to: 'requests#new_student_membership_request', as: :new_student_membership
  post 'student/membership/step/one', to: 'requests#student_membership_step_one_create', as: :student_membership_step_one
  put 'student/membership/update', to: 'requests#student_membership_update', as: :student_membership_update
  get 'advertise', to: 'requests#advertise'
  post 'advertise_enquiry_create', to: 'requests#advertise_enquiry_create'

  resources :requests, :path => '', only:[:none] do 
    collection do
      get 'aboutus', to: 'faqs/aboutus_faqs'
      get 'bulk_order_faqs', to: 'faqs/bulkOrder_faqs'
      get 'publish_faqs', to: 'faqs/publish_faqs'
      get 'student_membership_faqs', to: 'faqs/student_membership_faqs'
      get 'forget_password_faqs', to: 'faqs/forget_password_faqs'
      get 'publisher_author_guidelines_faqs', to: 'faqs/publisher_author_guidelines_faqs'
      get 'shipping_delivary_faqs', to: 'faqs/shipping_delivary_faqs'
      get 'request_book_faqs', to: 'faqs/request_book_faqs'
      get 'shipping_rates_faqs', to: 'faqs/shipping_rates_faqs'
      get 'careers', to: 'requests#careers'
      get 'contact_us', to: 'requests#contact_us'
      post 'contact_us', to: 'requests#create_new_contact_request'
      get 'bulk_order_discounts', to: 'requests#bulk_order_discounts'
      post 'create_bulk_order_discount', to: 'requests#create_bulk_order_discount'
    end
  end

  resources :carts, only:[:none] do
    member do 
      put 'add_to_cart'
      put 'increase_quantity'
      put 'decrease_quantity'
      put 'decrease_quantity_cart'
      put 'increase_quantity_cart'
    end

    collection do 
      get 'show', as: :cart
    end 
  end

  resources :orders, only: [:create]
  get 'check_out', to: 'orders#check_out', as: :check_out

  resources :reviews, only:[:update, :destroy, :create]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
