require_relative 'boot'

require 'rails/all'
require 'iconv'
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Dropingo
  class Application < Rails::Application
  	config.autoload_paths += %W(#{config.root}/app/workers)
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
