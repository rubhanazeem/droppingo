# config valid only for current version of Capistrano
lock '3.6.1'

set :application, 'droppingo'
set :repo_url, 'git@bitbucket.org:phaedra/droppingo.git'

set :branch, 'master'

set :deploy_to, '/www/sites/droppingo'
set :scm, :git

set :format, :pretty
set :log_level, :debug
set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system', 'solr')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }
set :rails_env, 'production'

# Default value for keep_releases is 5
set :keep_releases, 2

set :rvm_ruby_version, '2.3.0'
# set :rvm_custom_path, '/usr/local/rvm'

namespace :deploy do

  task :setup_solr_data_dir do
    on roles(:app), in: :sequence, wait: 5 do
      execute "mkdir -p #{shared_path}/solr/data"
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'tmp:cache:clear'
      # end
      # invoke 'delayed_job:restart'
    end
  end
  # after "deploy:update_code", "deploy:migrate"
  after :publishing, 'deploy:restart'
  after :finishing, 'deploy:cleanup'
end


namespace :solr do

  %w[start stop].each do |command|
    desc "#{command} solr"
    task command do
      on roles(:app) do
        solr_pid = "#{shared_path}/solr/pids/production/sunspot-solr-production.pid"
        if command == "start" or (test "[ -f #{solr_pid} ]" and test "kill -0 $( cat #{solr_pid} )")
          within current_path do
            # execute 
            with rails_env: fetch(:rails_env, 'production') do
              execute :bundle, 'exec', "sunspot-solr #{command} --pid-dir=/www/sites/droppingo/shared/solr"
            end
          end
        end
      end
    end
  end

  desc "restart solr"
  task :restart do
    invoke 'solr:stop'
    invoke 'solr:start'
  end

  after 'deploy:finished', 'solr:restart'

  desc "reindex the whole solr database"
  task :reindex do
    invoke 'solr:stop'
    on roles(:app) do
      execute :rm, "-rf #{shared_path}/solr/data"
    end
    invoke 'solr:start'
    on roles(:app) do
      within current_path do
        with rails_env: fetch(:rails_env, 'production') do
          info "Reindexing Solr database"
          execute :bundle, 'exec', :rake, 'sunspot:solr:reindex[,,true]'
        end
      end
    end
  end
end

# For capistrano
namespace :sidekiq do
  task :quiet do
    on roles(:app) do
      p '-'*100
      p capture("pgrep -f 'sidekiq' | xargs kill -USR1")
    end
  end
  task :start do
    on roles(:app) do
      p '='*100
      execute 'bundle exec sidekiq -c 10 -e production -L log/sidekiq.log -d'
      p capture("ps aux | grep sidekiq")
    end
  end
end

