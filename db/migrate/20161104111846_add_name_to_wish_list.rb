class AddNameToWishList < ActiveRecord::Migration[5.0]
  def change
    add_column :wish_lists, :name, :string
  end
end
