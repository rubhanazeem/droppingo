class AddYouTubeToSocialShare < ActiveRecord::Migration[5.0]
  def change
    add_column :social_shares, :youtube_link, :string
  end
end
