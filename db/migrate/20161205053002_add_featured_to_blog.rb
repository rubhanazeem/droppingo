class AddFeaturedToBlog < ActiveRecord::Migration[5.0]
  def change
    add_column :blogs, :featured, :boolean, default: false
  end
end
