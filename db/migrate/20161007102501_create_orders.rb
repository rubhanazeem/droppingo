class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.references :user, foreign_key: true
      t.decimal :grand_total, :precision => 8, :scale => 2
      t.string :delivary_method
      t.string :delivary_address

      t.timestamps
    end
  end
end
