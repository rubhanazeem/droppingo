class CreateStudentProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :student_profiles do |t|
      t.references :user, foreign_key: true
      t.date :date_of_birth
      t.integer :gender
      t.string :name
      t.string :email
      t.string :password
      t.string :institution_name
      t.string :current_educational_level
      t.string :area_of_study
      t.string :year_of_study
      t.string :course_length
      t.string :student_id
      t.string :mobile_no

      t.timestamps
    end
  end
end
