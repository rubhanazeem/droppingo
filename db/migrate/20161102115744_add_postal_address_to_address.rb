class AddPostalAddressToAddress < ActiveRecord::Migration[5.0]
  def change
    add_column :addresses, :postal_address, :text
  end
end
