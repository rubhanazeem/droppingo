class CreatePublishRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :publish_requests do |t|
      t.string :full_name
      t.string :job_title
      t.string :city
      t.string :email
      t.string :mobile_no
      t.string :primary_subj_area
      t.text :project_summary

      t.timestamps
    end
  end
end
