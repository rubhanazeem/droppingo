class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.references :user, foreign_key: true
      t.string :city
      t.string :country
      t.string :postal_code
      t.string :state

      t.timestamps
    end
  end
end
