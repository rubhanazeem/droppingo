class CreateBookRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :book_requests do |t|
      t.string :book_title
      t.string :author
      t.string :full_name
      t.string :email
      t.string :mobile_no
      t.string :comments

      t.timestamps
    end
  end
end
