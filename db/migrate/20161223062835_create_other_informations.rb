class CreateOtherInformations < ActiveRecord::Migration[5.0]
  def change
    create_table :other_informations do |t|
      t.references :bulk_order_discount, foreign_key: true
      t.string :book_title
      t.string :author
      t.string :publisher
      t.integer :quantity

      t.timestamps
    end
  end
end
