class AddMessageToBulkOrderDiscount < ActiveRecord::Migration[5.0]
  def change
    add_column :bulk_order_discounts, :message, :text
  end
end
