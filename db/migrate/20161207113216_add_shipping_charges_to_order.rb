class AddShippingChargesToOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :shipping_charges, :integer
  end
end
