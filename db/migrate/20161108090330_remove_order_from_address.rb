class RemoveOrderFromAddress < ActiveRecord::Migration[5.0]
  def change
    remove_reference :addresses, :order, foreign_key: true
  end
end
