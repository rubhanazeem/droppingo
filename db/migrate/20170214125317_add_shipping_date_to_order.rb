class AddShippingDateToOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :shipping_date, :date
  end
end
