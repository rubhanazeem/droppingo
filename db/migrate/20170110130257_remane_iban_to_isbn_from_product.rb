class RemaneIbanToIsbnFromProduct < ActiveRecord::Migration[5.0]
  def change
  	rename_column :products, :iban, :isbn
  end
end
