class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|
      t.references :user, foreign_key: true
      t.references :product, foreign_key: true
      t.text :body
      t.decimal :rating, :precision => 2, :scale => 1

      t.timestamps
    end
  end
end
