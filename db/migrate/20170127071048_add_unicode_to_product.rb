class AddUnicodeToProduct < ActiveRecord::Migration[5.0]
  def change
  	execute "alter table products convert to character set utf8 collate utf8_general_ci;"
  end
end
