class CreateSocialShares < ActiveRecord::Migration[5.0]
  def change
    create_table :social_shares do |t|
      t.string :fb_link
      t.string :twitter_link
      t.string :instagram_link
      t.string :pinterest_link

      t.timestamps
    end
  end
end
