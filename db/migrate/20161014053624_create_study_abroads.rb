class CreateStudyAbroads < ActiveRecord::Migration[5.0]
  def change
    create_table :study_abroads do |t|
      t.string :full_name
      t.string :interested_study_field
      t.string :current_education
      t.string :city
      t.string :email
      t.string :mobile_no
      t.text :message

      t.timestamps
    end
  end
end
