class CreateProductsWishLists < ActiveRecord::Migration[5.0]
  def change
    create_table :products_wish_lists do |t|
      t.references :product, foreign_key: true
      t.references :wish_list, foreign_key: true

      t.timestamps
    end
  end
end
