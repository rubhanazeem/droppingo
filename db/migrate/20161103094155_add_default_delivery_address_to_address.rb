class AddDefaultDeliveryAddressToAddress < ActiveRecord::Migration[5.0]
  def change
    add_column :addresses, :default_address, :boolean, default: false
  end
end
