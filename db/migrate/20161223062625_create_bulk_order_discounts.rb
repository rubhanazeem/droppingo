class CreateBulkOrderDiscounts < ActiveRecord::Migration[5.0]
  def change
    create_table :bulk_order_discounts do |t|
      t.string :full_name
      t.string :company
      t.string :city
      t.string :country
      t.string :phone_number
      t.string :email

      t.timestamps
    end
  end
end
