class AddNewFieldsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :mobile_no, :string
    add_column :users, :title, :string
  end
end
