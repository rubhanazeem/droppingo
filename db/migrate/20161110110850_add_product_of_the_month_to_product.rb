class AddProductOfTheMonthToProduct < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :product_of_the_month, :boolean, default: false
  end
end
