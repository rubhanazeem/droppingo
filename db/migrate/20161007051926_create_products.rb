class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :title
      t.string :author
      t.decimal :price, :precision => 8, :scale => 2
      t.string :description
      t.string :iban
      t.date :published_date

      t.timestamps
    end
  end
end
