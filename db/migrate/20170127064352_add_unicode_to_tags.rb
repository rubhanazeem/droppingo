class AddUnicodeToTags < ActiveRecord::Migration[5.0]
  def change
  	execute "alter table tags convert to character set utf8 collate utf8_general_ci;"
  end
end
