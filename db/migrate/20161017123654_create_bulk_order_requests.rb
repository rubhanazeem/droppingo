class CreateBulkOrderRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :bulk_order_requests do |t|
      t.string :full_name
      t.string :company
      t.string :city
      t.string :country
      t.string :phone_no
      t.string :email
      t.string :book_title
      t.string :author
      t.string :publisher
      t.integer :quantity
      t.text :message

      t.timestamps
    end
  end
end
