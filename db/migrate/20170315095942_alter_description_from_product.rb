class AlterDescriptionFromProduct < ActiveRecord::Migration[5.0]
  def change
  	change_table :products do |t|
  		t.change :description, :text
  	end
  end
end
