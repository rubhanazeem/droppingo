Category.destroy_all
AdminUser.destroy_all
Tag.destroy_all
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

general = Category.create("name" => "General Books")


engineering = general.children.create("name" => "Engineering")
ce = engineering.children.create("name" => "Civil Engineering")
ee = engineering.children.create("name" => "Electrical Engineering")
me = engineering.children.create("name" => "Mechanical Engineering")
chem = engineering.children.create("name" => "Chemical Engineering")
power = engineering.children.create("name" => "Power Engineering")
electronic = engineering.children.create("name" => "Radio & Electronics")
energy = engineering.children.create("name" => "Energy")
industrial = engineering.children.create("name" => "Industrial Engineering")
mechatronics = engineering.children.create("name" => "Mechatronics & Control Engineering")
metallurgy = engineering.children.create("name" => "Metallurgy & Materials Science")
petrolium = engineering.children.create("name" => "Petroleum Engineering")
telecommunication = engineering.children.create("name" => "Telecommunication Engineering")
textile = engineering.children.create("name" => "Textile & Fashion")
total = engineering.children.create("name" => "Total Quality Management")
transportation = engineering.children.create("name" => "Transportation Engineering")
urban = engineering.children.create("name" => "Urban Planning")
auto = engineering.children.create("name" => "Automotive & Transportation Engineering")


agriculture = general.children.create("name" => "Agriculture")
agriculture_forestry = agriculture.children.create("name" => "Agriculture & Forestry")
fisheries = agriculture.children.create("name" => "Fisheries")
food_science = agriculture.children.create("name" => "Food Science & Technology")
plant_pathology = agriculture.children.create("name" => "Plant Pathology")
nutrition = agriculture.children.create("name" => "Nutrition")


arts_architecture = general.children.create("name" => "Arts & Architecture")
architecture = arts_architecture.children.create("name" => "Architecture")
fine_art = arts_architecture.children.create("name" => "Fine Arts")
architecture_eng = arts_architecture.children.create("name" => "Architecture Engineering")


biotechnology = general.children.create("name" => "Biotechnology")


it = general.children.create("name" => "IT")
comp_sc = it.children.create("name" => "Computer Science")
grap_design = it.children.create("name" => "Graphic Design")
networking = it.children.create("name" => "Networking")


management = general.children.create("name" => "Management Sciences")
acc = management.children.create("name" => "Accounting")
com = management.children.create("name" => "Commerce")
bank_finance = management.children.create("name" => "Banking & Finance")
business_manag = management.children.create("name" => "Business & Managemen")
city_plan = management.children.create("name" => "City & Regional Planning")
manag_sci = management.children.create("name" => "Management Sciences")
advertising_manag = management.children.create(name: "Advertising & Marketing")
policy = management.children.create("name" => "Public Administration & Public Policy")


mathematics = general.children.create(name: "Mathematics General")


media_studies = general.children.create(name: "Media Studies")


life_sci = general.children.create(name: "Physical & Life Sciences")
bot = life_sci.children.create(name: "Botany")
chem = life_sci.children.create(name: "Chemistry")
eco = life_sci.children.create(name: "Ecology")
environmental_science = life_sci.children.create(name: "Environmental Science")
soil_science = life_sci.children.create(name: "Soil Science")
wildlife = life_sci.children.create(name: "Wildlife")
geology = life_sci.children.create(name: "Geology")
phy_edu = life_sci.children.create(name: "Physical Education")
phy = life_sci.children.create(name: "Physics")


social_sci = general.children.create(name: "Social Sciences")
anthropology = social_sci.children.create(name: "Anthropology")
archeology = social_sci.children.create(name: "Archeology")
economics = social_sci.children.create(name: "Economics")
education = social_sci.children.create(name: "Education")
english_language = social_sci.children.create(name: "English Language")
english_literature = social_sci.children.create(name: "English Literature")
geography = social_sci.children.create(name: "Geography")
history = social_sci.children.create(name: "History")
home_economics = social_sci.children.create(name: "Home Economics")
international_relations = social_sci.children.create(name: "International Relations")
islamic_studies = social_sci.children.create(name: "Islamic Studies")
law = social_sci.children.create(name: "Law")
library_science = social_sci.children.create(name: "Library Science")
mass_communication = social_sci.children.create(name: "Mass Communication")
pakistan_studies = social_sci.children.create(name: "Pakistan Studies")
philosophy = social_sci.children.create(name: "Philosophy")
political_science = social_sci.children.create(name: "Political Science")
research_methodology = social_sci.children.create(name: "Research Methodology")
sociology = social_sci.children.create(name: "Sociology")
social_work = social_sci.children.create(name: "Social Work")
special_education = social_sci.children.create(name: "Special Education")
statistics = social_sci.children.create(name: "Statistics")
women_studies = social_sci.children.create(name: "Women Studies")


references_others = general.children.create(name: "References & Others")
other = references_others.children.create(name: "Others")
references = references_others.children.create(name: "References")



acad_and_text = Category.create("name" => "Academic & Textbooks")

category_division = acad_and_text.children.create(name: "Academic & Textbooks")
business_finance = category_division.children.create(name: "Business & Finance")
communication_journalism = category_division.children.create(name: "Communication & Journalism")
computer_science = category_division.children.create(name: "Computer Science")
education = category_division.children.create(name: "Education")
eng = category_division.children.create(name: "Engineering")
humanities = category_division.children.create(name: "Humanities")
law = category_division.children.create(name: "Law")
medicine_health_sciences = category_division.children.create(name: "Medicine & Health Sciences")
guides_references = category_division.children.create(name: "Guides & References")
science_mathematics = category_division.children.create(name: "Science & Mathematics")
social_sciences = category_division.children.create(name: "Social Sciences")
o_levels = category_division.children.create(name: "O-Levels")
a_levels = category_division.children.create(name: "A-Levels")
intermediate = category_division.children.create(name: "Intermediate")
undergraduate = category_division.children.create(name: "Undergraduate")
post_graduate = category_division.children.create(name: "Post-Graduate")
miscellaneous = category_division.children.create(name: "Miscellaneous")
essays = category_division.children.create(name: "Essays")
urdu_literature = category_division.children.create(name: "Urdu & Urdu Literature")




fiction = Category.create("name" => "Fiction")
category_division_fict = fiction.children.create(name: "Fiction")
classic = category_division_fict.children.create(name: "Classic")
comic = category_division_fict.children.create(name: "Comic")
drama = category_division_fict.children.create(name: "Drama")
horror = category_division_fict.children.create(name: "Horror")
humor = category_division_fict.children.create(name: "Humor")
suspense_thriller = category_division_fict.children.create(name: "Mystery & Suspense Thriller")
romance = category_division_fict.children.create(name: "Romance")
science_category_division_fict = category_division_fict.children.create(name: "Science Fiction")
short_stories = category_division_fict.children.create(name: "Short Stories")
action_adventure = category_division_fict.children.create(name: "Action & Adventure")
afsanay = category_division_fict.children.create(name: "Afsanay")
childrens_books = category_division_fict.children.create(name: "Children's Books")
column = category_division_fict.children.create(name: "Column")
fic_others = category_division_fict.children.create(name: "Others")
novels = category_division_fict.children.create(name: "Novels")



non_fiction = Category.create("name" => "Non Fiction")
category_division_non_fict = non_fiction.children.create(name: "Non Fiction")
arts_photography = category_division_non_fict.children.create(name: "Arts & Photography")
biographies_memoirs = category_division_non_fict.children.create(name: "Biographies & Memoirs")
coffee_table = category_division_non_fict.children.create(name: "Coffee Table")
cooking_food = category_division_non_fict.children.create(name: "Cooking & Food")
music_films_entertainment = category_division_non_fict.children.create(name: "Music Films & Entertainment")
sports = category_division_non_fict.children.create(name: "Sports")
travel_books = category_division_non_fict.children.create(name: "Travel & Guide Books")
miscellaneous = category_division_non_fict.children.create(name: "Miscellaneous")
dictionaries_encyclopedias = category_division_non_fict.children.create(name: "Dictionaries & Encyclopedias")
poetry = category_division_non_fict.children.create(name: "Poetry")
children_books = category_division_non_fict.children.create(name: "Children's Books")
beauty_fashion = category_division_non_fict.children.create(name: "Beauty & Fashion")
non_fic_novels = category_division_non_fict.children.create(name: "Novels")
religion = category_division_non_fict.children.create(name: "Religion")
criminology = category_division_non_fict.children.create(name: "Criminology")
europe = category_division_non_fict.children.create(name: "Europe")
government_politics = category_division_non_fict.children.create(name: "Government & Politics")
research = category_division_non_fict.children.create(name: "Research")
science_technology = category_division_non_fict.children.create(name: "Science & Technology")



islamic_books = Category.create("name" => "Islamic Books")
category_division_islamic = islamic_books.children.create(name: "Islamic Books")
islamic_history = category_division_islamic.children.create(name: "Islamic History")
islamic = category_division_islamic.children.create(name: "Islamic")
others = category_division_islamic.children.create(name: "Others")



exam_preparation = Category.create("name" => "Exam Preperation")
category_division_exam = exam_preparation.children.create(name: "CSS, PMS, PCS")
gmat = category_division_exam.children.create(name: "GMAT")
gat = category_division_exam.children.create(name: "GAT")
sat = category_division_exam.children.create(name: "SAT")
mcat = category_division_exam.children.create(name: "MCAT")
gre_test = category_division_exam.children.create(name: "GRE")
lcat = category_division_exam.children.create(name: "LCAT")
anthropology = category_division_exam.children.create(name: "Anthropology")
arabic = category_division_exam.children.create(name: "Arabic")
botany = category_division_exam.children.create(name: "Botany")
business_administration = category_division_exam.children.create(name: "Business Administration")
current_aff = category_division_exam.children.create(name: "Current Affairs")
econ = category_division_exam.children.create(name: "Economics")
eng_test_prep = category_division_exam.children.create(name: "English")
environment_sci = category_division_exam.children.create(name: "Environmental Science")
gender_studies = category_division_exam.children.create(name: "Gender Studies")
gen_knowledge = category_division_exam.children.create(name: "General Knowledge")
gen_science = category_division_exam.children.create(name: "General Science")
geog = category_division_exam.children.create(name: "Geography")
gov_policy = category_division_exam.children.create(name: "Governance & Public Policies")
history_exam_prep = category_division_exam.children.create(name: "History")
international_relations_exam_prep = category_division_exam.children.create(name: "International Relations")
islamic_studies_exam = category_division_exam.children.create(name: "Islamic Studies")
law_exam = category_division_exam.children.create(name: "Law")
m_com_exam = category_division_exam.children.create(name: "Mass Communications")
other_exam = category_division_exam.children.create(name: "Others")
pkst = category_division_exam.children.create(name: "Pakistan Studies")
persian_exam = category_division_exam.children.create(name: "Persian")
philosophy_exam = category_division_exam.children.create(name: "Philosophy")
pol_sci = category_division_exam.children.create(name: "Political Science")
psyc = category_division_exam.children.create(name: "Psychology")
public_adminintration = category_division_exam.children.create(name: "Public Adminstration")
punjabi_exam = category_division_exam.children.create(name: "Punjabi")
social_work_exam = category_division_exam.children.create(name: "Social Work")
socialo = category_division_exam.children.create(name: "Sociology")
solved_papers = category_division_exam.children.create(name: "Solved Papers")
syllabus_exam = category_division_exam.children.create(name: "Syllabus")
town_plan = category_division_exam.children.create(name: "Town Planning & Urban Management")
urdu_exam = category_division_exam.children.create(name: "Urdu")
nts = category_division_exam.children.create(name: "NTS")


medical_science = Category.create("name" => "Medical Sciences")
category_division_med = medical_science.children.create(name: "Medical Sciences")
anesthesia = category_division_med.children.create(name: "Anesthesia")
med_biochemistry = category_division_med.children.create(name: "Biochemistry")
med_Biology = category_division_med.children.create(name: "Biology")
med_Biomedical_engineering = category_division_med.children.create(name: "Biomedical Engineering")
med_anatomy = category_division_med.children.create(name: "Anatomy")
cancer = category_division_med.children.create(name: "Cancer")
cardiology = category_division_med.children.create(name: "Cardiology")
community_medicine = category_division_med.children.create(name: "Community Medicine")
dentistry = category_division_med.children.create(name: "Dentistry")
dermatology = category_division_med.children.create(name: "Dermatology")
endocrinology = category_division_med.children.create(name: "Endocrinology")
ent = category_division_med.children.create(name: "ENT")
forensic_medicine = category_division_med.children.create(name: "Forensic Medicine")
gastroentrology = category_division_med.children.create(name: "Gastroentrology")
genetics = category_division_med.children.create(name: "Genetics")
haematology = category_division_med.children.create(name: "Haematology")
medical_education = category_division_med.children.create(name: "Medical Education")
medicine = category_division_med.children.create(name: "Medicine")
microbiology = category_division_med.children.create(name: "Microbiology")
neurology_neurosurgery = category_division_med.children.create(name: "Neurology & Neurosurgery")
nursing = category_division_med.children.create(name: "Nursing")
obstetrics_gyenaecology = category_division_med.children.create(name: "Obstetrics & Gyenaecology")
opthalmology = category_division_med.children.create(name: "Opthalmology")
orthopaedics = category_division_med.children.create(name: "Orthopaedics")
paediatrics = category_division_med.children.create(name: "Paediatrics")
pathology = category_division_med.children.create(name: "Pathology")
pharmacology = category_division_med.children.create(name: "Pharmacology")
physiology = category_division_med.children.create(name: "Physiology")
physiotherapy = category_division_med.children.create(name: "Physiotherapy")
psychiatry = category_division_med.children.create(name: "Psychiatry")
psychology = category_division_med.children.create(name: "Psychology")
radiology = category_division_med.children.create(name: "Radiology")
respiratory_disease = category_division_med.children.create(name: "Respiratory Disease")
surgery = category_division_med.children.create(name: "Surgery")
urology_nephrology = category_division_med.children.create(name: "Urology & Nephrology")
veterinary = category_division_med.children.create(name: "Veterinary")
zoology_entomology = category_division_med.children.create(name: "Zoology & Entomology")
other_medical = category_division_med.children.create(name: "Other Medical")

# test = Category.create("name" => "Test")


Tag.create! [
	{name: "Engineering"},
	{name: "Medical"},
	{name: "Literature"},
	{name: "General"}
]