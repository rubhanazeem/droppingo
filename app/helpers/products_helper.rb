module ProductsHelper

	def render_breadcrumb_links
    breadncrumb = "<a href=#{products_path(:category_id => @category.id)}> #{@category.name}</a>"
    @cat = @category
    while !get_parent(@cat).nil?
      breadncrumb = "<a href=#{products_path(:category_id => @cat.id)}> #{@cat.name} </a>" + "<p style='margin-right: 5px;'> / </p> " + breadncrumb
    end
    breadncrumb
  end

  def get_parent(cat)
    @cat = @cat.parent
  end

  

end
