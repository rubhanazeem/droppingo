module ApplicationHelper
	def number_of_children(id)
    root = Category.find id 
    children = root.children 
    number_of_children_sub_children = children.count
    children.each do |child|
    	count = number_of_sub_child(child)
      number_of_children_sub_children = number_of_children_sub_children + count.to_i
    end  
    number_of_children_sub_children
  end

  def number_of_sub_child(id)
    first_child = Category.find id
    number_of_sub_childern = first_child.children.count if first_child.children.present?
  end

  def include_all_categories(id)
  	root = Category.find id 
    children = root.children 
    categories_arr = Array.new
    children.each do |child|
    	categories_arr.push(child.id)
    	sub_children = child.children.pluck(:id)
    	categories_arr.push(sub_children)
    end  
    return Category.where(id: categories_arr.compact.flatten.uniq.sort).order(:id)
  end
end
