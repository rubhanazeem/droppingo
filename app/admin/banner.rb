ActiveAdmin.register Banner do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :avatar#, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
	form html: { multipart: true } do |f|
	  f.inputs "Details" do
	    f.input :avatar, hint: f.object.avatar? ? image_tag(f.object.avatar.url, height: '100') : content_tag(:span, "Upload JPG/PNG/JPEG image")
	  end
	  f.actions
	end

	index do
		selectable_column
		column :id
		column "Image" do |banner|
		  image_tag banner.avatar.url(:thumb), class: 'my_image_size'
		end
	  actions
	end

end
