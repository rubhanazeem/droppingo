ActiveAdmin.register BookRequest do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
	index do
	  column :id
	  column :book_title
	  column :author
	  column :full_name
	  column :email
	  column :mobile_no
	  actions
	end

  filter :book_title
  filter :author
  filter :full_name
  filter :email

  controller do

	  private

	  def book_request_params
	    params.require(:book_request).permit(:book_title, :email, :author, :full_name, :mobile_no, :comments)
	  end
	end
end
