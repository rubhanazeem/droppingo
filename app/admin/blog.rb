ActiveAdmin.register Blog do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :body, :avatar, :title, :featured
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
form html: { multipart: true } do |f|
  f.inputs "Blog" do
  	f.input :title
  	f.input :body, :input_html => { :class => 'tinymce_editor', :rows => 20, :cols => 30}, :label => 'Body'
    f.input :featured, as: :radio
  	f.input :avatar, hint: f.object.avatar? ? image_tag(f.object.avatar.url, height: '100') : content_tag(:span, "Upload JPG/PNG/JPEG image")
  end
  f.actions
end

index do
	column :id
	column :title
	column :featured
  actions
end

filter :body
filter :title

end
