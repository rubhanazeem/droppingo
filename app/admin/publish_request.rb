ActiveAdmin.register PublishRequest do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
	index do
	  column :id
	  column :email
	  column :full_name
	  column :primary_subj_area
	  column :city
	  column :mobile_no
	  actions
	end

  filter :email
  filter :full_name
  filter :city
  filter :mobile_no

  controller do

	  private

	  def publish_request_params
    	params.require(:publish_request).permit(:full_name, :primary_subj_area, :job_title, :city, :email, :mobile_no, :project_summary)
	  end
	end

end
