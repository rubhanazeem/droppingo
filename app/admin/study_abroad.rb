ActiveAdmin.register StudyAbroad do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
	index do
	  column :id
	  column :email
	  column :full_name
	  column :interested_study_field
	  column :current_education
	  column :city
	  column :mobile_no
	  actions
	end

  filter :email
  filter :full_name
  filter :city
  filter :mobile_no

  controller do

	  private

	  def study_abroad_params
    	params.require(:study_abroad).permit(:full_name, :interested_study_field, :current_education, :city, :email, :mobile_no, :message)
	  end
	end

end
