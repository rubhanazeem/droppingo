ActiveAdmin.register Category do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :parent_id, :cover
#
form html: { multipart: true } do |f|
  f.inputs "Details" do
    f.input :name
		f.input :parent_id, :include_blank => true, allow_blank: true, as: :select, collection: Category.all.map {|u| [u.name, u.id]}, input_html: { class: 'chosen-select' }, :label => "Parent"
		# if f.object.parent.present?
		# 	f.input for: ["root", f.object.parent.parent || Category.new] do |r| 
	  #      r.input "root", :include_blank => true, allow_blank: true, as: :select, collection: Category.all.map {|u| [u.name, u.id]}, input_html: { class: 'chosen-select', id: "root" }, :label => "Rooot"
	  #    end
	  #  end
		# f.input "root", hint: f.object.parent.parent? ? input_tag() :include_blank => true, allow_blank: true, as: :select, collection: Category.all.map {|u| [u.name, u.id]}, input_html: { class: 'chosen-select', id: "root" }, :label => "Rooot"
		f.input :cover, hint: f.object.cover? ? image_tag(f.object.cover.url, height: '100') : content_tag(:span, "Upload JPG/PNG/JPEG image")
	end
	f.actions
end

show do
  attributes_table do
    row :id
    row :name
    row 'Parent' do 
    	category.parent.name
    end
    row 'Root' do 
    	category.root.name
    end
  end
end

index do
	selectable_column
	column :id
  column :name
  column :parent
  actions
end

filter :name
filter :parent

controller do
	def destroy
		@cat = Category.find params[:id]
		@cat.custom_delete
		flash[:notice] = "Category deleted successfully!"
		redirect_to admin_categories_path
	end
end

end
