ActiveAdmin.register Product do
# require 'stalker'
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#

permit_params :title, :author, :price, :description, :isbn, :avatar, :publisher, :featured, :pages, :edition,
							 categor_attributes: [:id, :name, :parent_id], tag_attributes: [:name]


form html: { multipart: true } do |f|
  f.inputs "Details" do
    f.input :title
    f.input :edition
    f.input :author
    f.input :price
    # f.input :published_date, as: :date_picker, :input_html => {:class => 'admin-date-picker'}
    f.input :isbn
    f.input :publisher
    f.input :pages
    f.input :avatar, hint: f.object.avatar? ? image_tag(f.object.avatar.url, height: '100') : content_tag(:span, "Upload JPG/PNG/JPEG image")
    f.input :categories, :include_blank => false, allow_blank: false, as: :select, :multiple => true, collection: Category.all.map {|u| [u.name, u.id]}, input_html: { class: 'chosen-select' }, :label => "Category"
    f.input :featured, as: :radio
    f.input :product_of_the_month, as: :radio
    f.input :description, :input_html => { :class => 'tinymce_editor', :rows => 20, :cols => 50}, :label => 'Description'
    # f.has_many :tags do |tag|
    f.input :tags, as: :select, :input_html => {:id => 'input-tags3', :class => 'tags-product'}, :multiple => true
    # end
  end
  f.actions
end

index do
	selectable_column
	column :id
	column :source
  column :title
  column :author
  column :price
  # column :published_date
  column :isbn
  actions
end

filter :categories
filter :tags
filter :title
filter :author
filter :price
filter :isbn
filter :publisher
filter :source
filter :featured
filter :product_of_the_month

controller do
	def create
		params.permit!
		tag_ids = Array.new
		params[:product][:tag_ids].each do |id|
			if id.present?
				tag = Tag.find_by(id: id)
				if tag.present?
					tag_ids << id
				else
					tag = Tag.create(name: id)
					tag.save
					tag_ids << tag.id
				end
			end
		end
		# if params[:product][:product_of_the_month]
  #     @products = Product.where(product_of_the_month: true)
  #     @products.each do |prod|
  #       prod.product_of_the_month = false
  #       prod.save
  #     end
  #   end
		params[:product][:tag_ids] = tag_ids
		@product = Product.create(params[:product])
		if @product.save
			flash.notice = "Product created successfully!"
			redirect_to admin_product_path(@product)
		else
			flash.alert = @product.errors.full_messages
			redirect_to admin_product_path(@product)
		end
	end

	def update
		params.permit!
		tag_ids = Array.new
		params[:product][:tag_ids].each do |id|
			if id.present?
				tag = Tag.find_by(id: id)
				if tag.present?
					tag_ids << id
				else
					tag = Tag.create(name: id)
					tag.save
					tag_ids << tag.id
				end
			end
		end
		# if params[:product][:product_of_the_month]
  #     @products = Product.where(product_of_the_month: true)
  #     @products.each do |prod|
  #       prod.product_of_the_month = false
  #       prod.save
  #     end
  #   end
		params[:product][:tag_ids] = tag_ids
		@product = Product.find(params[:id])
		@product.update_attributes(params[:product])
		if @product.save
			flash.notice = "Product updated successfully!"
			redirect_to admin_product_path(@product)
		else
			flash.alert = @product.errors.full_messages
			redirect_to admin_product_path(@product)
		end
	end
  
end
	action_item do 
		link_to "Import products", import_products_admin_products_path, method: :get 
	end
	collection_action :import_products, method: :get do

	end

	collection_action :update_products, method: :post do
		if File.extname(params[:product][:csv_file].original_filename) == ".xlsx"
			system "mv #{params[:product][:csv_file].path} #{Rails.root.join('product.xlsx')}"
			file_name = 'product.xlsx'
			# spreadsheet = Roo::Excelx.new(params[:product][:csv_file].path)
		elsif File.extname(params[:product][:csv_file].original_filename) == ".csv"
			system "mv #{params[:product][:csv_file].path} #{Rails.root.join('product.csv')}"
			file_name = 'product.csv'
			# spreadsheet = Roo::CSV.new(params[:product][:csv_file].path)
		end
		UploadProductWorker.perform_async(file_name)
		# Stalker.enqueue('upload.products', :file_name => file_name)
		# ===================== manual ===================
		# i = spreadsheet.first_row + 1
		# while i <= spreadsheet.last_row
		# 	p = Product.new(source: spreadsheet.row(i)[0], isbn: spreadsheet.row(i)[1], title: spreadsheet.row(i)[2], author:  spreadsheet.row(i)[3], publisher:  spreadsheet.row(i)[4], language:  spreadsheet.row(i)[5], price: spreadsheet.row(i)[12])
		#   pages = spreadsheet.row(i)[10]
		#   p.pages = pages if pages.present?
		#   p.published_date = Date.strptime("{#{spreadsheet.row(i)[9]}, 1, 1 }", "{ %Y, %m, %d }") if spreadsheet.row(i)[9].present?
	  #   begin
	  #      url = spreadsheet.row(i)[13]
	  #      p.avatar = url
	  #    rescue => e
	  #   end
		#   root = Category.find_by(name: spreadsheet.row(i)[8].downcase.strip) 
		#   child = root.children.where(name: spreadsheet.row(i)[7].downcase.strip).first if root.present?
		#   sub_child = child.children.where(name: spreadsheet.row(i)[6].downcase.strip).first if child.present?
		#   p.categories << [root, child, sub_child].compact
		#   # ============= Add tags =================
		#   if spreadsheet.row(i)[15].present?
		# 	  spreadsheet.row(i)[15].split(",").each do |name|
		# 	  	tag = Tag.find_by(name: name.downcase.strip)
		# 	  	if tag.nil?
		# 	  		tag = Tag.create(name: name.downcase.strip)
		# 	  	end
		# 	  	p.tags << tag
		# 	  end
		# 	end
		#   p.save
		#   i = i + 1
		# end
		flash.notice = "Products are being uploading!"
		redirect_to admin_products_path
	end
end


 # spreadsheet = Roo::Excelx.new(params[:product][:csv_file].path)