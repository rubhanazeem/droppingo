ActiveAdmin.register BulkOrderDiscount do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

	index do
		selectable_column
		column :id
	  column :full_name
	  column :company
	  column :city
	  column :country
	  column :phone_number
	  column :email
	  actions
	end

	filter :full_name
	filter :company
	filter :city
	filter :country
	filter :phone_number
	filter :email

end
