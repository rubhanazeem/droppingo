ActiveAdmin.register SocialShare do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :fb_link, :twitter_link, :pinterest_link, :instagram_link, :youtube_link
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
form html: { multipart: true } do |f|
  f.inputs "Social Share Links" do
  	f.input :fb_link, :label => 'Facebook Link'
  	f.input :twitter_link, :label => 'Twitter Link'
    f.input :pinterest_link, :label => 'Pinterest Link'
  	f.input :instagram_link, :label => 'Instagram Link'
  	f.input :youtube_link, :label => 'Youtube Link'
  end
  f.actions
end

index do
	column :id
	column :fb_link
	column :twitter_link
	column :pinterest_link
	column :instagram_link
	column :youtube_link
  actions
end

# disable filters
config.filters = false

controller do
	def create
		SocialShare.create(social_params)
		SocialShare.first.destroy if SocialShare.count > 1
		flash[:notice] = "New links added."
		redirect_to admin_social_shares_path
	end

	def update
		s = SocialShare.find params[:id]
		s.update(update_params)
		s.save
		flash[:notice] = "Updated successfull."
		redirect_to admin_social_share_path(s.id)
	end

	private
	def social_params
		params.require(:social_share).permit(:fb_link, :twitter_link, :pinterest_link, :instagram_link, :youtube_link)
	end
	def update_params
		params.require(:social_share).permit(:fb_link, :twitter_link, :pinterest_link, :instagram_link, :youtube_link)
	end
end

end
