ActiveAdmin.register Order do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :shipping_date, :status
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

	form html: { multipart: true } do |f|
	  f.inputs "Details" do
	  	f.input :shipping_date, as: :datepicker, datepicker_options: { min_date: Date.today, max_date: "+1Y" }
	    f.input :status, :include_blank => false, allow_blank: false, as: :select, collection: ['Pending', 'Delivered'], :label => "Status"
	  end
	  f.actions
	end

	index do
		selectable_column
		column :id
	  column :grand_total
	  column :delivary_method
	  column "Address" do |i|
	  	if i.address.present?
	    	i.address.postal_address+", "+i.address.city+", "+i.address.country
	    else
	    	i.delivary_address
	  	end
	  end
	  column :created_at
	  column :shipping_charges
	  column :shipping_date
	  column :status
	  actions
	end

	filter :grand_total
	filter :delivary_method
	filter :created_at
	filter :shipping_charges
	filter :status

	show do
	  attributes_table do
	    row :id
	    row 'User' do 
	    	order.user.first_name+" "+order.user.last_name
	    end
	    row :grand_total 
	    row :delivary_method
	    row 'Address' do 
	    	if order.address.present?
	    		order.address.postal_address+", "+order.address.city+", "+order.address.country
	    	else
	    		order.delivary_address
	    	end
	    end
	    row :number
	    row :message
	    table_for order.products do
	      column "Title" do |product|
	        product.title
	      end
	      column "Author" do |product|
	        product.author
	      end
	      column "Price" do |product|
	        product.price
	      end
	      # column :appointment_date
	    end
	    row :status
	    row :shipping_date
	  end
	  active_admin_comments
	end
end
