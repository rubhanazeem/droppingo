class UploadProductWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false


  def perform(file_name)
  	line_no = 0
  	if file_name == 'product.csv'
  		file = File.open(Rails.root.join('product.csv'))
  		spreadsheet = Roo::CSV.new(file.path)
  	else
  		file = File.open(Rails.root.join('product.xlsx'))
  		spreadsheet = Roo::Excelx.new(file.path)
  	end
  	i = spreadsheet.first_row + 1
    while i <= spreadsheet.last_row
    	line_no = i
			p = Product.new(source: spreadsheet.row(i)[0], isbn: spreadsheet.row(i)[1], title: spreadsheet.row(i)[2], author:  spreadsheet.row(i)[3], publisher:  spreadsheet.row(i)[4], language:  spreadsheet.row(i)[5], price: spreadsheet.row(i)[12])
		  pages = spreadsheet.row(i)[10]
		  p.pages = pages if pages.present?
		  p.published_date = Date.strptime("{#{spreadsheet.row(i)[9]}, 1, 1 }", "{ %Y, %m, %d }") if spreadsheet.row(i)[9].present?
	    begin
	      url = spreadsheet.row(i)[13]
	      stream = open(url, "rb")
        newfile = Tempfile.new.tap do |file|
			    file.binmode
			    IO.copy_stream(stream, file)
			    stream.close
			    file.rewind
			  end
				p.avatar = newfile
      rescue => e
	    end
		  root = Category.find_by(name: spreadsheet.row(i)[8]) 
		  child = root.children.where(name: spreadsheet.row(i)[7]).first if root.present?
		  sub_child = child.children.where(name: spreadsheet.row(i)[6]).first if child.present?
		  p.categories << [root, child, sub_child].compact
		  # ============= Add tags =================
		  if spreadsheet.row(i)[15].present?
			  spreadsheet.row(i)[15].split(",").each do |name|
			  	tag = Tag.find_by(name: name.downcase.strip)
			  	if tag.nil?
			  		tag = Tag.create(name: name.downcase.strip)
			  	end
			  	p.tags << tag
			  end
			end
		  p.save
		  p.index
			Sunspot.commit
			unless url.blank?
      	newfile.close if newfile
      end
		  i = i + 1
		end
		# system "bundle exec rake sunspot:solr:reindex"
		# system "rm '#{Rails.root.join(file_name)}'"
		# system "chmod -R 777 /www/sites/droppingo/releases/"
  rescue Exception => error
  	message = "#{error.message} at record = #{line_no}"
    ErrorMailer.error_logs(message).deliver_later
    Sidekiq.logger.warn "#{'='*10} #{error} at record = #{line_no} #{'='*10}"
  end
end

# logger.debug "Here's some info: #{hash.inspect}"