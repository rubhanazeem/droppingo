class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # prepend_before_filter :require_no_authentication, only: [:cancel ]
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :find_cart
  # skip_before_action :verify_authenticity_token

  helper_method :sub_categories

  def sub_categories(id = nil)
    @categories = Category.get_all_subchildren(id)
  end

  protected

  def configure_permitted_parameters
    added_attrs = [:first_name, :last_name, :email, :password, :password_confirmation, :remember_me, :current_password]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

  def authenticate_user!(options={})
    if user_signed_in?
      super(options)
    else
      redirect_to root_path, :info => 'Please login to view dashboard!'
      ## if you want render 404 page
      ## render :file => File.join(Rails.root, 'public/404'), :formats => [:html], :status => 404, :layout => false
    end
  end

  private

  def find_cart
    if current_user.present?
      if session[:cart].present?
        @cart = Cart.find session[:cart]["id"]
        @cart.user = current_user
        @cart.save
        session[:cart] = nil
        @cart
      else
        @cart = current_user.cart ||= Cart.create()
      end
    else
      session[:cart] ||= Cart.create()
      @cart = Cart.find session[:cart]["id"]
    end
  end

end
