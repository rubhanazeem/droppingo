class SessionsController < Devise::SessionsController
  skip_before_filter :require_no_authentication
  skip_before_filter :verify_signed_out_user, only: :destroy
  before_action { flash.clear }
  # skip_before_filter :validate_auth_token
  # before_filter :validate_auth_token, :except => :create
  include Devise::Controllers::Helpers
  respond_to :html, :js

  def create
    resource = User.find_for_database_authentication(:email => params[:user][:email])
    return failure unless resource
    if resource.valid_password?(params[:user][:password])
      sign_in(:user, resource)
    	set_flash_message!(:info, :signed_in)
    	respond_with resource, location: after_sign_in_path_for(resource)
      flash[:info] = "Signed in successfully."
  	else
  		return failure
    end
  end

  def destroy
  	signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    set_flash_message! :info, :signed_out if signed_out
    yield if block_given?
    # reset_session
    respond_to_on_destroy
  end

  def failure
    flash[:alert] = "Invalid email/password combination."
  end
end
