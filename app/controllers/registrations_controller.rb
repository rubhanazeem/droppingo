class RegistrationsController < Devise::RegistrationsController
	skip_before_action :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }
  skip_before_action :require_no_authentication
  skip_before_action :authenticate_user!, raise: false
  before_filter :validate_auth_token, :except => [:create, :update, :edit]
  respond_to :html, :js

  # POST /resource
  def create
    build_resource(sign_up_params)
    @password = params[:user][:password]
    @password_confirmation = params[:user][:password_confirmation]
    @email = params[:user][:email]
    @email_confirmation = params[:user][:email_confirmation]
    resource.save
    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message! :info, :signed_up
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
        flash[:info] = "Signed-up successfully!"
      else
        set_flash_message! :info, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end
    # binding.pry
  end

  def edit
  end

  def update
    if params[:user][:current_password] != nil
      if update_resource(resource, account_update_params)
        bypass_sign_in(resource)
        flash[:info] = "User updated successfully!"
        redirect_to user_profile_url
      else
        flash[:alert] = resource.errors.full_messages
        redirect_to :back
      end
    else
      if resource.update_without_password(account_update_params.except(:current_password))
        bypass_sign_in(resource)
        flash[:info] = "User updated successfully!"
        redirect_to user_profile_url
      else
        flash[:alert] = resource.errors.full_messages
        redirect_to :back
      end
    end
  end


  private

  def sign_up_params
    params.require(:user).permit(:first_name, :last_name, :email, :email_confirmation, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:title, :mobile_no, :first_name, :last_name, :email, :email_confirmation, :password, :password_confirmation, :current_password)
  end
end