class CartsController < ApplicationController
	before_action :set_product, only: [:add_to_cart, :increase_quantity, :decrease_quantity, :decrease_quantity_cart, :increase_quantity_cart]

	def show
		@cart = current_user.cart if current_user.present?
		@grand_total = 0
		@cart.products.group(:id).each do |product|
			@grand_total = (@cart.products.group(:id).count[product.id] * product.price) + @grand_total
		end
		if @cart.products.count < 1
			redirect_to root_path
			flash[:alert] = "Your cart is empty at this moment."
		else
			@order = Order.new
		end
		@default_address = current_user.addresses.where(default_address: true).first if current_user.present?
		if @grand_total < 1000
			@shipping_charges = 150
		else
			@shipping_charges = 0
		end
	end

	def add_to_cart
		@cart_item = @cart.cart_items.find_or_initialize_by(product_id: @product.id)
		if @cart_item.present? && @cart_item.quantity.present?
			i = @cart_item.quantity
			while i > 0
				@cart.products << @product
				i = i - 1
			end
			@cart_item.destroy
			flash[:info] = "Product added successfully!" if i > 0
		else
			@cart.products << @product
      flash[:info] = "Product added successfully!"
		end

	end

	def decrease_quantity_cart
		@cart_product = CartsProduct.where(product_id: @product.id, cart_id: @cart.id).last.destroy
	end

	def increase_quantity_cart
		@cart.products << @product
	end

	def increase_quantity
		@cart_item = @cart.cart_items.find_or_initialize_by(product_id: @product.id)
		if @cart_item.quantity.present?
			@cart_item.quantity = @cart_item.quantity + 1
		else
			@cart_item.quantity = 2
		end
	end

	def decrease_quantity
		@cart_item = @cart.cart_items.find_or_initialize_by(product_id: @product.id)
		if @cart_item.quantity.present?
			@cart_item.quantity = @cart_item.quantity - 1 if @cart_item.quantity >= 1
		else
			@cart_item.quantity = 1
		end

		# respond_to do |format|
  #     format.js { render :nothing => true }
  #     format.html
  #   end
	end

	private

	def set_product
		@product = Product.find params[:id]
	end
end
