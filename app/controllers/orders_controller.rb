class OrdersController < ApplicationController
	def create
		@order = current_user.orders.create(order_params)
		@address = Address.create(address_params)
		if @address.save
			@order.address = @address
			@order.products = @cart.products
			if @order.delivary_method == 'Shipping Option 1' && @order.grand_total < 1000
				@order.shipping_charges = 150
			elsif @order.delivary_method == 'Shipping Option 2' && @order.grand_total <= 5000
				@order.shipping_charges = 250
			elsif @order.delivary_method == 'Shipping Option 2' && @order.grand_total > 5000
				@order.shipping_charges = 350
			end
			@order.status = 'Pending'
			if @order.save
				current_user.cart.products.destroy_all
				flash[:info] = "Your order has been placed successfully!"
			end
		end
	end

	def check_out
		@cart = current_user.cart
		@grand_total = 0
		@cart.products.group(:id).each do |product|
			@grand_total = (@cart.products.group(:id).count[product.id] * product.price) + @grand_total
		end
		if @cart.products.count < 1
			redirect_to root_path
			flash[:alert] = "Your cart is empty at this moment."
		else
			@order = Order.new
		end
	end

	
	private

	def order_params
		params.require(:order).permit(:grand_total, :status, :delivary_method , :delivary_address, :number, :message, address: [:title, :first_name, :last_name, :postal_address])
	end

	def address_params
		params.require(:address).permit(:title, :first_name, :last_name, :postal_address,:city, :state, :country)
	end

end
