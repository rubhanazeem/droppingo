class ProductsController < ApplicationController
	before_action :set_product, only:[:show]
	

	def index
		if params[:category_id].present?
			@category = Category.find params[:category_id]
		else
			@category = nil
		end
		@product_sort_by = params[:sort_by]
		count = Product.count
		@search = Sunspot.search(Product) do
	    fulltext params[:search] do 
	    	fields(:title, :author)
	    end
			# order_by(:title, :desc) if params[:sort_by] == "Title"
			paginate :page => 1, :per_page => count
			if params[:sort_by] == "Price Low-High"
	    	order_by(:price, :asc)
	    elsif params[:sort_by] == "Price High-Low"
	    	order_by(:price, :desc)
	    elsif params[:sort_by] == "Title A-Z"
	    	order_by(:title, :asc)
	    elsif params[:sort_by] == "Title Z-A"
	    	order_by(:title, :desc)
	    end
	    facet :price, :range => 0..5000, :range_interval => 1000
      with(:price, Range.new(*params[:price_range].split("-").map(&:to_i))) if params[:price_range].present?
	    with(:category_ids, params[:category_id]) if params[:category_id].present?
	  end
	  @price_range = params[:price_range] 
	  @view_type = params[:view_type].to_i
	  if params[:per_page].present?
	  	@products = Kaminari.paginate_array(@search.results).page((params[:page] rescue 1)).per(params[:per_page])
	  	@products_per_page = params[:per_page]
	  else
	  	@products = Kaminari.paginate_array(@search.results).page((params[:page] rescue 1)).per(20)
	  end
	end

	def show
		@product
		tag = @product.tags.last
		category = @product.categories.last
		if tag.present? 
			@same_tag_products = tag.products.where.not(id: @product.id).last(6)
		end
		@cart_item = @cart.cart_items.find_or_initialize_by(product_id: @product.id)
		if @cart_item.present? && @cart_item.quantity.present?
			@quantity = @cart_item.quantity
		else
			@quantity = 1
		end
		# current user review
		@review = @product.reviews.where(user_id: current_user.id) if current_user.present?
		
		# total reviews
		@all_reviews = @product.reviews
		@all_reviews_rating = @product.reviews.pluck(:rating)
		@sum = @all_reviews_rating.sum
		@rating = @sum/@all_reviews_rating.count if @all_reviews_rating.present?

		@social = SocialShare.first
	end

	private
	def set_product
		@product = Product.find(params[:id])
	end

end
