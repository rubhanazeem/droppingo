class RequestsController < ApplicationController
	include RequestsHelper
	before_action :get_social_networks

	def bulk_order_info	
	end

	# ==============study abroad================
	def study_abroad_new
		@new_request = StudyAbroad.new
	end

	def study_abroad_create
		@new_request = StudyAbroad.create(study_abroad_params)
	end
	# ==============study abroad================

	# ==============publish request=============
	def publish_requests_new
  	@new_request = PublishRequest.new
	end

	def publish_requests_create
		@new_request = PublishRequest.create(publish_request_params)
	end
	# ==============publish request=============

	# ==============book request================
	def book_request_new
		@new_request = BookRequest.new
	end

	def book_request_create
		@new_request = BookRequest.create(book_request_params)
	end
	# ==============book request================

	# ==============advertise================
	def advertise
		@main_view = params[:main_view]
	end

	def advertise_enquiry_create
		@advertise = Advertise.create(advertise_params)
	end

	# ==============advertise================


	# ==============student membership==========
	def new_student_membership_request
		@profile = StudentProfile.new
	end

	def student_membership_step_one_create
		@profile = StudentProfile.create(student_membership_params)
		if current_user.present? && !current_user.student_profile.present?
			current_user.user_type = 1
			@profile.user = current_user
			flash[:info] = "Profile created Successfully!"
		elsif !current_user.present?
			@name = params[:student_profile][:name].split(" ")
			resource.first_name = @name[0]
			resource.last_name = @name[1]
			resource.email = params[:student_profile][:email]
			resource.email_confirmation = params[:student_profile][:email]
			resource.password = params[:student_profile][:password]
			resource.password_confirmation = params[:student_profile][:password]
			resource.user_type = 1
			if resource.save
				sign_in(resource)
        flash[:info] = "Signed-up successfully!"
        @resource_created = true
      else
      	@resource_created = false
			end
			@profile.user = resource if resource.persisted?
		end
	end

	def student_membership_update 
		@profile = StudentProfile.find params[:student_profile][:profile_id]
		@profile.update(student_membership_update_params)
	end

	# ==============student membership==========
	def aboutus
		render 'requests/faqs/aboutus'
	end

	def bulk_order_faqs
		render 'requests/faqs/bulkOrder_faqs'
	end

	def publish_faqs
		render 'requests/faqs/publish_faqs'
	end

	def student_membership_faqs
		render 'requests/faqs/student_membership_faqs'
	end

	def forget_password_faqs
    render 'requests/faqs/forgot_password_faqs'
  end

  def publisher_author_guidelines_faqs
  	render 'requests/faqs/publisher_author_guidelines_faqs'
  end

  def shipping_delivary_faqs
  	render 'requests/faqs/shipping_delivary_faqs'
  end

  def request_book_faqs
  	render 'requests/faqs/request_book_faqs'
  end

  def shipping_rates_faqs
  	render 'requests/faqs/shipping_rates_faqs'
  end

  def terms_and_conditions
  	render 'requests/faqs/terms_and_conditions'
  end

  def privacy_policy
  	render 'requests/faqs/privacy_policy'
  end

  def careers
  end

  def contact_us
  	@contact = Contact.new
  end

  def create_new_contact_request
  	@contact = Contact.new(contact_params)
  	if @contact.save
  		flash[:notice] = 'Your request has been submitted successfully. We\'ll contact you soon'
      ContactMailer.contact(@contact).deliver_later
  		redirect_to :back 
  	else
  		flash[:alert] = @contact.errors.full_messages.join('<br>'.html_safe)
  		render :contact_us
  	end
  end

  def bulk_order_discounts
  	render 'requests/bulk_order_discounts'
  end

  def create_bulk_order_discount
  	@bulk_order_discount = BulkOrderDiscount.new(bulk_order_discounts_params)
  	other_information = params[:other_information]
  	@others = []
  	other_information.each do |info|
  		other_info = OtherInformation.new()
  		other_info.book_title = info[:book_title]
  		other_info.author = info[:author]
  		other_info.publisher = info[:publisher]
  		other_info.quantity = info[:quantity]
  		other_info.save
  		@others.push(other_info)
  	end
  	@bulk_order_discount.other_informations = @others
  	if @bulk_order_discount.save
  		flash[:info] = "Your request has been submitted successfully!"
  		redirect_to :back
  	else
  		flash[:alert] = @bulk_order_discount.errors.full_messages.join('<br>'.html_safe)
  		render :bulk_order_discounts
  	end
  end

	private

	def study_abroad_params
    params.require(:study_abroad).permit(:full_name, :interested_study_field, :current_education, :city, :email, :mobile_no, :message)
  end

  def publish_request_params
    params.require(:publish_request).permit(:full_name, :primary_subj_area, :job_title, :city, :email, :mobile_no, :project_summary)
  end

  def book_request_params
    params.require(:book_request).permit(:book_title, :author, :email, :full_name, :mobile_no, :comments)
  end

  def student_membership_params
  	params.require(:student_profile).permit(:date_of_birth, :gender , :name, :email, :password, :institution_name, :current_educational_level, :area_of_study, :year_of_study, :course_length, :student_id, :mobile_no)
  end

  def student_membership_update_params
  	params.require(:student_profile).permit(:date_of_birth, :gender , :name, :email, :password, :institution_name, :current_educational_level, :area_of_study, :year_of_study, :course_length, :student_id, :mobile_no)
  end

  def advertise_params
  	params.require(:advertise).permit(:name, :email, :contact_no, :message, :company)
  end

  def bulk_order_discounts_params
  	params.require(:bulk_order_discount).permit(:full_name, :company, :city, :country, :phone_number, :email, :message)
  end

  def other_information_params
  	params.require(:other_information).permit(:book_title, :author, :publisher, :quantity)
  end

  def contact_params
  	params.require(:contact).permit(:name, :email, :phone_no, :company, :message)
  end

  def get_social_networks
  	@social = SocialShare.first
  end
end
