class HomeController < ApplicationController
  def index
    featured_categories = Category.where(name: ["Academic & Textbooks", "Exam Preparation"])
  	@featured_products = Product.joins(:categories_products).where("products.featured = ? AND categories_products.category_id NOT IN (?)",true, featured_categories.collect(&:id)).uniq
    academic_categories = Category.where(name: "Academic & Textbooks")
    @academic_text_products = Product.joins(:categories).where("products.featured = ? AND categories.name IN (?)", true, academic_categories.collect(&:name)).uniq.first(16)
  	exam_prep_categories = Category.where(name: "Exam Preparation")
  	@competative_exam_products = Product.joins(:categories).where("products.featured = ? AND categories.name IN (?)", true, exam_prep_categories.collect(&:name)).uniq.first(16)
  	@featured_blogs = Blog.where(featured: true).last(4)
  	@social = SocialShare.first
  end
end
