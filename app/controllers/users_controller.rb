class UsersController < ApplicationController
	before_action :set_product, only:[:add_to_wish_list]
  before_filter :authenticate_user!



# ===============User Profile===============
	def show_profile
		@profile = current_user.student_profile if current_user.student_profile.present?
  	@user = current_user
	end

  def edit_profile
  	@profile = current_user.student_profile
  	@user = current_user
  end
# ===============User Profile=================


# ===============User orders==================
def show_orders
  if params[:page] == 'all'
    @orders = current_user.orders
  else
    @orders = current_user.orders.page((params[:page] || 1)).per(2)
  end 
end

def order_details
  @order = Order.find params[:id]
  @counts = @order.products.group(:id).count
end
# ===============User orders==================

# ===========User wishlist====================
  def user_lists
    @lists = current_user.wish_lists if current_user.wish_lists.present?  
  end

  def create_list
    @list = current_user.wish_lists.create(name: params[:wish_list][:name])
    if @list.save
      flash[:info] = 'List created successfully!'
      redirect_to :back
    else
      flash[:alert] = 'Please enter name for list!'
      redirect_to :back
    end
  end

  def edit_list
    @list = current_user.wish_lists.where(id: params[:id]).first
  end

  def remove_from_list
    @list = current_user.wish_lists.where(id: params[:list]).first
    @list.products.delete(Product.find(params[:id]))
    if @list.save
      flash[:info] = 'Book has been deleted from your wishlist!'
      redirect_to :back
    else
      flash[:alert] = 'Something went wrong!'
      redirect_to :back
    end
  end

  def add_to_wish_list
  	if params[:wish_list][:list_ids].present?
      params[:wish_list][:list_ids].each do |id|
        list = WishList.find id
        list.products << @product if !list.products.where(id: @product.id).present?  
        list.save
      end
      return flash.now[:info] = 'Book has been added to your wishlist!'
  	else
  		return flash.now[:alert] = 'Please select any list!'
  	end
  end
# ===========User wishlist====================

  def user_dashboard
    @user = current_user
  end

  # ===============user addressbook===============
  def address_book
    @addresses = current_user.addresses
  end

  def new_address
    @address = Address.new
    respond_to do |format|
      format.js {render layout: false} 
    end
  end

  def add_new_address
    @address = Address.create(address_params)
    current_user.addresses << @address
    flash[:info] = "New address added successfully!"
  end

  def edit_address
    @address = Address.find params[:id]
    respond_to do |format|
      format.js {render layout: false} 
    end
  end

  def update_address
    @address = Address.find params[:id]
    @address.update(address_update_params)
    flash[:info] = "Address updated successfully!"
    if @address.save
      redirect_to request.referer  
    end
  end

  def destroy_address
    @address = Address.find params[:id]
    @address.destroy
    flash[:info] = "Address removed successfully!"
    respond_to do |format|
      format.js {redirect_to request.referer} 
    end
  end

  def set_default_address
    @address = current_user.addresses.where(id: params[:id]).first
    @previous_addr = current_user.addresses.where(default_address: true).first
    @previous_addr.default_address = false if @previous_addr.present?
    @address.default_address = true
    @address.save
    @previous_addr.save if @previous_addr.present?
    flash[:info] = "Address updated successfully!"
    respond_to do |format|
      format.js {redirect_to :back
      }
    end
  end
  # ===============user addressbook===============



  # ===============student club===================

  def student_club
    @profile = current_user.student_profile if current_user.student_profile.present?
  end

  def edit_student_club
    @profile = current_user.student_profile 
  end

  # ===============student club===================


  # ===============reviews===================
  def show_reviews
    @reviews = current_user.reviews
  end

  # ===============reviews===================
  private

  def set_product
  	@product = Product.find params[:wish_list][:product_id]
  end

  def address_params
    params.require(:address).permit(:first_name, :last_name, :title, :postal_address, :city, :state, :country)
  end

  def address_update_params
    params.require(:address).permit(:first_name, :last_name, :title, :postal_address, :city, :state, :country)
  end
end
