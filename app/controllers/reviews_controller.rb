class ReviewsController < ApplicationController
	before_action :set_review, only:[:update, :destroy]

	def create
		@review = current_user.reviews.create(review_params)
		@review.rating = params[:score]
		@product = Product.find params[:id]
		@review.product = @product
		flash[:info] = "Your review added successfully!"
	end

	def update
		@review.update(update_review_params)
		@review.rating = params[:score]
	end

	def destroy
		@review.destroy
		flash[:info] = "Your review removed successfully!"
		redirect_to :back
	end

	private
	def set_review
		@review = Review.find params[:id]
	end

	def update_review_params
		params.require(:review).permit(:body, :rating)
	end

	def review_params
		params.require(:review).permit(:body, :rating)
	end
end
