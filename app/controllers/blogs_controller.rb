class BlogsController < ApplicationController
	before_action :get_social_networks
	
	def index
		@blogs = Blog.all
	end
	
  def show
  	@blog = Blog.find params[:id]
  end

  private
  def get_social_networks
  	@social = SocialShare.first
  end
end
