class Devise::PasswordsController < DeviseController
  prepend_before_action :require_no_authentication, only:[:create]
  # Render the #edit only if coming from a reset password email link
  # append_before_action :assert_reset_token_passed, only: :edit

  def create
    if User.where(email: params[:user][:email]).present?
      @user = User.send_reset_password_instructions(:email => params[:user][:email])
      if successfully_sent?(@user)
      	flash[:info] = "Password update instructions has been sent to your email."
        flash.discard(:notice)
        redirect_to :back
      else
      	flash[:info] = @user.errors.full_messages
        redirect_to :back
      end
    else
      flash[:alert] = "This email is not registered!"
      redirect_to :back
    end
  end

  def edit
    self.resource = resource_class.new
    set_minimum_password_length
    resource.reset_password_token = params[:reset_password_token]
  end

  def update
    self.resource = resource_class.reset_password_by_token(resource_params)
    yield resource if block_given?

    if resource.errors.empty?
      if Devise.sign_in_after_reset_password
        flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
        set_flash_message!(:info, flash_message)
        sign_in(:user, resource)
      else
        set_flash_message!(:notice, :updated_not_active)
      end
      return redirect_to '/'
    else
      set_minimum_password_length
      respond_with resource
    end
  end

  
end
