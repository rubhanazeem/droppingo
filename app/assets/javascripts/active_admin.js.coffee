#= require active_admin/base
#= require jquery_ujs
#= require chosen-jquery
#= require selectize
#= require tinymce
$(document).ready ->
  tinyMCE.init
    mode: "textareas"
    theme: "modern"
    menubar: false
    content_style: "div {width: 80%;}"
    editor_selector: "tinymce_editor"
    automatic_uploads: false
    browser_spellcheck: true
    allow_html_in_named_anchor: true
    fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt'
    font_formats: 'Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n'
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | fontsizeselect'
  return

$ ->
  $('.chosen-select').chosen
    allow_single_deselect: true
    no_results_text: 'No results matched'
    width: '80%'

$ ->
  $('#input-tags3').selectize
    plugins: [ 'remove_button' ]
    delimiter: ','
    persist: false
    create: (input) ->
      {
        value: input
        text: input
      }