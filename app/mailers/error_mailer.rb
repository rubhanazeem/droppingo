class ErrorMailer < ApplicationMailer
	default from: "noreply@droppingo.com"

	def error_logs(error)
		@error = error
		mail(to: 'ali.muhammad97@gmail.com', subject: "File upload error logs (Droppingo)",
      date: Time.now)
	end
end
