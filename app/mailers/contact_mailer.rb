class ContactMailer < ApplicationMailer
	default from: "noreply@droppingo.com"

	def contact(message)
		@contact = message
		mail(to: 'ali.muhammad97@gmail.com', subject: "New message from droppingo contact",
      date: Time.now)
	end
end
