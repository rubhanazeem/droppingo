class BookRequest < ApplicationRecord
	validates :email, presence: true
	validates :book_title, presence: true
	validates :author, presence: true
	validates :full_name, presence: true
	validates :mobile_no, presence: true
end
