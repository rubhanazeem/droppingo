class StudentProfile < ApplicationRecord
	enum gender:[:male, :female]
  belongs_to :user
  validates :email, presence: true
	validates :name, presence: true
	# validates :date_of_birth, presence: true
	# validates :gender, presence: true

	# validates_presence_of :institution_name, :on => [ :update ]
	# validates_presence_of :current_educational_level, :on => [ :update ]
	# validates_presence_of :area_of_study, :on => [ :update ]
end
