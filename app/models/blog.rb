class Blog < ApplicationRecord
	validates :body, presence: true
	has_attached_file :avatar, styles: { medium: "300x300#", thumb: "100x100#" }, default_url: "/assets/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  def self.default_scope
    order(id: :desc)
  end
end
