class OtherInformation < ApplicationRecord
  belongs_to :bulk_order_discount

  validates :book_title, presence: true
	validates :author, presence: true 
	validates :publisher, presence: true 
	validates :quantity, presence: true
end
