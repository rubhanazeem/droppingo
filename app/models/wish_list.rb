class WishList < ApplicationRecord
  belongs_to :user

  has_many :products_wish_lists, dependent: :destroy
	has_many :products, through: :products_wish_lists

	validates :name, presence: true
	
end
