class Tag < ApplicationRecord
	has_many :products_tags, dependent: :destroy
	has_many :products, through: :products_tags
end
