class Cart < ApplicationRecord
  belongs_to :user
  has_many :carts_product, dependent: :destroy
	has_many :products, through: :carts_product

	has_many :cart_items, dependent: :destroy
end
