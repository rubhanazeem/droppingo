class ProductsWishList < ApplicationRecord
  belongs_to :product
  belongs_to :wish_list
end
