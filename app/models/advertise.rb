class Advertise < ApplicationRecord
	validates :name, presence: true
	validates :message, presence: true
	validates :company, presence: true

	# VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true#, format: { with: VALID_EMAIL_REGEX }

	# VALID_CONTACT_NO_REGEX = ^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$
	validates :contact_no, presence: true#, format: { with: VALID_CONTACT_NO_REGEX }
end
