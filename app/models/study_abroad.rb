class StudyAbroad < ApplicationRecord
	validates :email, presence: true
	validates :full_name, presence: true
	validates :interested_study_field, presence: true
	validates :current_education, presence: true
	validates :city, presence: true
	validates :mobile_no, presence: true
end
