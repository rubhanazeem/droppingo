class Order < ApplicationRecord
  belongs_to :user, required: true
  belongs_to :address, required: true
  has_many :orders_product, dependent: :destroy
	has_many :products, through: :orders_product
	accepts_nested_attributes_for :address

	validates :number, presence: true
end
