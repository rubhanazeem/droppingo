class Product < ApplicationRecord
	has_many :categories_products, dependent: :destroy
	has_many :categories, through: :categories_products

	has_many :carts_product, dependent: :destroy
	has_many :carts, through: :carts_product
	
	has_many :orders_product, dependent: :destroy
	has_many :order, through: :orders_product

	has_many :products_tags, dependent: :destroy
	has_many :tags, through: :products_tags

	has_many :cart_items, dependent: :destroy

	has_many :products_wish_lists, dependent: :destroy
	has_many :wish_lists, through: :products_wish_lists

  has_many :reviews, dependent: :destroy

	accepts_nested_attributes_for :categories
	accepts_nested_attributes_for :tags, allow_destroy: true

	has_attached_file :avatar, styles: { medium: "300x300#", thumb: "100x100#" }, default_url: "/assets/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  # validates :iban, uniqueness: true
  # after_save ThinkingSphinx::RealTime.callback_for(:product)
  after_create :add_parent_category

  default_scope { order(created_at: :desc) }

  searchable :auto_index => true do
    text :author
    text :title
    string :title    
    integer :category_ids, :multiple => true
    float :price
    text :categories do 
      categories.map(&:name)
    end
  end

  # add parent categories to product after creation
  def add_parent_category
    @categories = self.categories
    @cat = Array.new
    @categories.each do |category|
      @cat.push << category.id
      get_parent(category.parent)
    end
    self.category_ids = @cat.uniq
    self.save
  end


  def get_parent(cat)
	  return if cat.nil? 
	  @cat.push(cat.id)
	  get_parent(cat.parent)    
	end 

  def self.product_of_month(root)
    roots = Category.where(parent_id: nil).where.not(id: root.id)
    @product = root.products.joins(:categories).where('products.product_of_the_month = ? AND categories.name NOT IN (?)', true, roots.collect(&:name)).last
  end

end
