class Contact < ApplicationRecord
	VALID_PHONE_NUMBER_REGEX = /\A[0-9]*\z/
	validates :email, presence: true
	validates :phone_no, presence: true, 
                    format: { with: VALID_PHONE_NUMBER_REGEX }
end
