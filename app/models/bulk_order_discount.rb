class BulkOrderDiscount < ApplicationRecord
	has_many :other_informations, dependent: :destroy	
	VALID_PHONE_NUMBER_REGEX = /\A[0-9]*\z/
	validates :phone_number, presence: true, 
                    format: { with: VALID_PHONE_NUMBER_REGEX }
end
