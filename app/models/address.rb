class Address < ApplicationRecord
	# enum type: [:delivary_address, :personal_address]
  belongs_to :user
  has_many :orders

  validates :city, presence: true
	validates :country, presence: true
	# validates :postal_code, presence: true
	validates :title, presence: true
	validates :first_name, presence: true
	validates :last_name, presence: true
	validates :postal_address, presence: true
	validates :state, presence: true
end
