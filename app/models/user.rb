class User < ApplicationRecord
  enum user_type: [:simple, :student]
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :addresses, dependent: :destroy
  has_one :cart, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_one :student_profile, dependent: :destroy
  has_many :wish_lists, dependent: :destroy
  has_many :reviews, dependent: :destroy

  validates :email, 
	      # you only need presence on create
	      :presence => { :on => :create },
	      # and use confirmation to ensure they always match
	      :confirmation => true
end
