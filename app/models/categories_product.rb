class CategoriesProduct < ApplicationRecord
  belongs_to :product
  belongs_to :category
  
  # after_save ThinkingSphinx::RealTime.callback_for(:product, [:product])
end
