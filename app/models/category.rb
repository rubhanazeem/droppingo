class Category < ApplicationRecord
	extend ActsAsTree::TreeView
	# extend FriendlyId

	acts_as_tree order: "name"
	has_many :categories_products, dependent: :destroy
	has_many :products, through: :categories_products

	has_attached_file :cover, styles: { medium: "300x300#", thumb: "100x100#" }, default_url: "/assets/missing.png"
  validates_attachment_content_type :cover, content_type: /\Aimage\/.*\z/
	# friendly_id :name, use: :slugged
	# after_save ThinkingSphinx::RealTime.callback_for(:category)

	# default scope
	# def self.default_scope
	# 	Category.all.order(:name)
	# end

	def self.get_all_parents
		@parent_categories = Array.new
		root_categories = Category.where(parent_id: nil)
		(@parent_categories << root_categories).flatten!
		children = Category.where.not(parent_id: nil)
		children.each do |child|
			if child.children.first.present?
				@parent_categories << child
			end
		end
		@parent_categories.sort! { |a,b| a.name.downcase <=> b.name.downcase }
	end

	def self.get_all_subchildren(id = nil)
		if id.present?
			c = Category.find(id)
			if c.children.present?
			  @children = c.descendants  
			else
				@children = c.parent.children
			end 
		else
			@root_categories = Category.where(parent_id: nil)
			@root_categories.sort { |a,b| a.name.downcase <=> b.name.downcase }
		end
	end

  def custom_delete
    categories_products.each do |cp|
      cp.destroy
    end
    self.delete
  end

end
