class PublishRequest < ApplicationRecord
	validates :email, presence: true
	validates :full_name, presence: true
	validates :primary_subj_area, presence: true
	validates :job_title, presence: true
	validates :city, presence: true
	validates :mobile_no, presence: true
	validates :project_summary, presence: true
end
